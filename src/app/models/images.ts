export class Image{
	constructor(
		public id: number,
		public title: string,
		public picture: string,
		public album_id: number
		){}
}