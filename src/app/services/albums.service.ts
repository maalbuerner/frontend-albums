import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Album } from '../models/albums';
import { GLOBAL } from './global';

@Injectable()
export class AlbumService
{
	public url: string;

	constructor(private _http: HttpClient)
	{
		this.url = GLOBAL.url;
	}

	getAlbums()
	{
		return this._http.get(this.url+'albums');
	}

	getAlbum(id: number)
	{
		return this._http.get(this.url+'album/'+ id.toString());
	}

	addAlbum(album: Album)
	{
		let json = JSON.stringify(album);
		let params = json;
		let headers = new HttpHeaders({'Content-Type':'application/json'});

		return this._http.post<any>(this.url+'albums', params, {headers: headers});
	}

	editAlbum(id: number, album: Album)
	{
		let json = JSON.stringify(album);
		let params = json;
		let headers = new HttpHeaders({'Content-Type':'application/json'});

		return this._http.put<any>(this.url+'album/'+ id.toString(), 
			params, {headers: headers});
	}

	deleteAlbum(id: number)
	{
		return this._http.delete(this.url+'album/'+ id.toString());
	}
}