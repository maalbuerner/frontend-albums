import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Image } from '../models/images';
import { GLOBAL } from './global';

@Injectable()
export class ImageService
{
	public url: string;	

	constructor(private _http: HttpClient)
	{
		this.url = GLOBAL.url;
	}

	getApiUrl(segment = '')
	{
		return this.url + 'get-image/' + segment;
	}

	getImages(idAlbum: number = null)
	{
		if(idAlbum != null)
			return this._http.get(this.url+'images/'+idAlbum.toString());

		return this._http.get(this.url+'images');
	}

	getImage(id: number)
	{
		return this._http.get(this.url+'image/'+ id.toString());
	}

	addImage(image: Image)
	{
		let json = JSON.stringify(image);
		let params = json;
		let headers = new HttpHeaders({'Content-Type':'application/json'});

		return this._http.post<any>(this.url+'images', params, {headers: headers});
	}

	editImage(id: number, image: Image)
	{
		let json = JSON.stringify(image);
		let params = json;
		let headers = new HttpHeaders({'Content-Type':'application/json'});

		return this._http.put<any>(this.url+'image/'+ id.toString(), 
			params, {headers: headers});
	}

	deleteImage(id: number)
	{
		return this._http.delete(this.url+'image/'+ id.toString());
	}
}