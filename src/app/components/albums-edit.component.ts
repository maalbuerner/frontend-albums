import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { AlbumService } from '../services/albums.service';
import { Album } from '../models/albums';

@Component({
	selector: 'albums-edit',
	templateUrl: '../views/albums-add.html',
	providers: [AlbumService]
})

export class AlbumsEditComponent implements OnInit {
	public titulo: string;
	public album: Album;
	public errorMessage: any;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	description: AbstractControl; 

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _albumService: AlbumService,
			private fb: FormBuilder
		)
	{
		this.myForm = fb.group({
			'title': ['', Validators.required],
			'description': ['', Validators.required]
		});
		this.title = this.myForm.controls['title'];
		this.description = this.myForm.controls['description'];

		this.titulo = "Editar album";
	}

	ngOnInit(): void
	{
		console.log("Editar album.");
		this.album = new Album(0, "", "");
		this.getAlbum();
	}

	getAlbum()
 	{
		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._albumService.getAlbum(id).subscribe(
				result => {
					console.log(result[0]);
					this.album = result[0];

					if(!this.album)
					{
						alert('Error en el servidor');
						this._router.navigate(['/']);
					}
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}			
			);
		});
	}

	onSubmit(form: any): void
	{
		this.album.title = form.title;
		this.album.description = form.description;

		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._albumService.editAlbum(id, this.album).subscribe(
				result => {
					// console.log(result);

					if(!result)
					{
						alert('Error en el servidor');
						this._router.navigate(['/']);
					}
					else
					{
						this._router.navigate(['/album', id]);
					}
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
						this._router.navigate(['/']);
					}
				}			
			);
		});
	}

onDropdown(){}
}