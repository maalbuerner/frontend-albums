import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { ImageService } from '../services/images.service';
import { Image } from '../models/images';
import { GLOBAL } from '../services/global';

@Component({
	selector: 'images-edit',
	templateUrl: '../views/images-add.html',
	providers: [ImageService]
})

export class ImagesEditComponent implements OnInit {
	public titulo: string;
	public image: Image;
	public errorMessage: any;
	public is_edit: boolean;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	picture: AbstractControl;
	// album: AbstractControl;

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _imageService: ImageService,
			private fb: FormBuilder
		)
	{
		this.myForm = fb.group({
			'title': ['', Validators.required],
			'picture': ['']
			// 'album': ['', Validators.required]
		});
		this.title = this.myForm.controls['title'];
		this.picture = this.myForm.controls['picture'];
		// this.album = this.myForm.controls['album'];

		this.titulo = "Editar imagen";
		this.is_edit = true;
	}

	ngOnInit(): void
	{
		console.log("Editar imagen.");
		this.image = new Image(0, "", "", 0);
		this.getImage();
	}

	getImage()
	{
		this._route.params.forEach((params: Params) => {
			let id = params['id'];			
			
			this._imageService.getImage(id).subscribe(
				response => {
					this.image = <any>response;

					console.log(response);

					if(!this.image)
					{
						this._router.navigate(['/']);
					}
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}
			);
		});
	}


	onSubmit(form: any): void
	{
		// console.log(" form value: ", form);
		this.image.title = form.title;
		this.image.picture = form.picture;

		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._imageService.editImage(id, this.image).subscribe(
				response => {
					if(!response)
					{
						alert("Error en el servidor");
					}
					else
					{
						// subir la imagen
						if(!this.filesToUpload)
						{
							this._router.navigate(['/album', this.image.album_id]);
						}
						else
						{
							this.makeFileRequest(GLOBAL.url+'upload-image/'+id, 
								[], this.filesToUpload).then( 
									(result) => {
										this.resultUpload = result;
										this.image.picture = this.resultUpload.filename;
										this._router.navigate(['/album', this.image.album_id]);
									},
									(error) => {
										console.log(error);
									}
								);

							alert("Imagen editada correctamente");
						}
					}
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}
			);
		});

		// this.myForm.reset();
	}
	
	public filesToUpload: Array<File>;
	public resultUpload;

	fileChangeEvent(fileInput: any)
	{
		this.filesToUpload = <Array<File>>fileInput.target.files;
	}

	makeFileRequest(url: string, params: Array<string>, files: Array<File>)
	{
		return new Promise( (resolve, reject) => {
			var formData: any = new FormData();
			var xhr = new XMLHttpRequest();

			for (var i = 0; i < files.length; i++) {
				formData.append('image', files[i], files[i].name);
			}

			xhr.onreadystatechange = () => {
				if(xhr.readyState == 4)
				{
					if(xhr.status == 200)
					{
						resolve(JSON.parse(xhr.response));
					}
					else
					{
						reject(xhr.response);
					}
				}				
			}

			xhr.open('POST', url, true);
			xhr.send(formData);
		});
	}
}