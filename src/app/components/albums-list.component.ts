import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AlbumService } from '../services/albums.service';
import { Album } from '../models/albums';

@Component({
	selector: 'albums-list',
	templateUrl: '../views/albums-list.html',
	providers: [AlbumService]
})

export class AlbumsListComponent implements OnInit {
	public titulo: string;
	public albums: Album[];
	public errorMessage: any;
	public loading: boolean;
	public confirmado: number;

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _albumService: AlbumService
		)
	{
		this.loading = false;
		this.titulo = "Listado de albums";
	}

	ngOnInit(): void
	{
		console.log("Lista del albums okokok");
		this.getAlbums();
	}

	getAlbums()
	{
		this.loading = true;
		this._albumService.getAlbums().subscribe(
			result => {
				this.albums = <any>result;
				// console.log(this.albums);
				if(!this.albums)
				{
					alert('Error en el servidor');
				}

				this.loading = false;
			},
			error => {
				this.errorMessage = <any>error;

				if(this.errorMessage != null)
				{
					console.log(this.errorMessage);
				}
			}			
		);
	}

	onDeleteConfirm(id: number)
	{
		this.confirmado = id;
	}	

	onDeleteAlbum(id: number)
	{
		this._albumService.deleteAlbum(id).subscribe(
			result => {
				// console.log(this.albums);
				if(!result)
				{
					alert('Error en el servidor');
				}
				else
				{
					this.getAlbums();
				}
			},
			error => {
				this.errorMessage = <any>error;

				if(this.errorMessage != null)
				{
					console.log(this.errorMessage);
				}
			}			
		);
	}

	onCancelDeleteAlbum()
	{
		this.confirmado = null;
	}
}