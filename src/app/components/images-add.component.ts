import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { ImageService } from '../services/images.service';
import { Image } from '../models/images';

@Component({
	selector: 'images-add',
	templateUrl: '../views/images-add.html',
	providers: [ImageService]
})

export class ImagesAddComponent implements OnInit {
	public titulo: string;
	public image: Image;
	public errorMessage: any;
	public is_edit: boolean;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	picture: AbstractControl;
	// album: AbstractControl;

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _imageService: ImageService,
			private fb: FormBuilder
		)
	{
		this.myForm = fb.group({
			'title': ['', Validators.required],
			'picture': ['']
			// 'album': ['', Validators.required]
		});
		this.title = this.myForm.controls['title'];
		this.picture = this.myForm.controls['picture'];
		// this.album = this.myForm.controls['album'];

		this.titulo = "Agregar image";
		this.is_edit = false;
	}

	ngOnInit(): void
	{
		console.log("Agregar album.");
		this.image = new Image(0, "", "", 0);
	}

	onSubmit(form: any): void
	{
		// console.log(" form value: ", form);
		this.image.title = form.title;
		// this.image.picture = form.picture;

		this._route.params.forEach((params: Params) => {

			let album_id = params['album_id'];
			this.image.album_id = album_id;

			this._imageService.addImage(this.image).subscribe(
				response => {

					if(!response)
					{
						alert("Error en el servidor");
					}
					else
					{
						// alert("Imagen agregada correctamente");
						this._router.navigate(['/edit-image', response.insertId]);
					}
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}
			);
		});

		// this.myForm.reset();
	}

	fileChangeEvent(fileInput: any){}
}