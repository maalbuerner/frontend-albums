import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { AlbumService } from '../services/albums.service';
import { ImageService } from '../services/images.service';
import { Album } from '../models/albums';
import { Image } from '../models/images';

@Component({
	selector: 'albums-detail',
	templateUrl: '../views/albums-detail.html',
	providers: [AlbumService, ImageService] 
})

export class AlbumsDetailComponent implements OnInit {
	public album: Album;
	public images: Image[];
	public errorMessage: any;
	public loading: boolean;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	description: AbstractControl; 

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _albumService: AlbumService,
			public _imageService: ImageService,
			private fb: FormBuilder
		)
	{
		this.myForm = fb.group({
			'title': ['', Validators.required],
			'description': ['', Validators.required]
		});
		this.title = this.myForm.controls['title'];
		this.description = this.myForm.controls['description'];
	}

	ngOnInit(): void
	{
		console.log("ver album.");
		this.getAlbum();
	}

	getAlbum()
 	{
		this.loading = true;
		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._albumService.getAlbum(id).subscribe(
				result => {
					// console.log(result);
					this.album = <any>result;
					console.log(this.album);

					if(!this.album)
					{
						alert('Error en el servidor');
						this._router.navigate(['/']);
					}
					else
					{
						// Uso del servicio de imagen
						this._imageService.getImages(this.album.id).subscribe(
							response => {
								this.images = <any>response;

								if(!this.images)
									console.log("Album sin imagenes.");
							},
							error => {
								this.errorMessage = <any>error;

								if(this.errorMessage != null)
								{
									console.log(this.errorMessage);
								}
							}
						);
					}

					this.loading = false;
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}			
			);
		});
	}

}