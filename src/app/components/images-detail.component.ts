import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { ImageService } from '../services/images.service';
import { Image } from '../models/images';

@Component({
	selector: 'images-detail',
	templateUrl: '../views/images-detail.html',
	providers: [ImageService] 
})

export class ImagesDetailComponent implements OnInit {
	public image: Image;
	public errorMessage: any;
	public loading: boolean;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	description: AbstractControl; 

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			public _imageService: ImageService,
		){}

	ngOnInit(): void
	{
		console.log("Ver Imagen.");
		this.getImage();
	}

	getImage()
 	{
		this.loading = true;
		this._route.params.forEach((params: Params) => {

			let id = params['id'];

			this._imageService.getImage(id).subscribe(
				result => {
 					console.log(result);
					this.image = <any>result; 

					if(!this.image)
					{
						alert('Error en el servidor');
						this._router.navigate(['/']);
					}

					this.loading = false;
				},
				error => {
					this.errorMessage = <any>error;

					if(this.errorMessage != null)
					{
						console.log(this.errorMessage);
					}
				}			
			);
		});
	}

	public confirmado: any;

	onDeleteConfirm(id: number)
	{
		this.confirmado = id;
	}	

	onDeleteImage(id: number)
	{
		this._imageService.deleteImage(id).subscribe(
			result => {
				// console.log(this.albums);
				if(!result)
				{
					alert('Error en el servidor');
				}
				else
				{
					this._router.navigate(['/album', this.image.album_id]);
				}
			},
			error => {
				this.errorMessage = <any>error;

				if(this.errorMessage != null)
				{
					console.log(this.errorMessage);
				}
			}			
		);
	}

	onCancelDeleteImage()
	{
		this.confirmado = null;
	}
}