import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import {
	FormBuilder,
	FormGroup,
	AbstractControl,
	Validators
} from '@angular/forms';

import { AlbumService } from '../services/albums.service';
import { Album } from '../models/albums';

declare var $: any;

@Component({
	selector: 'albums-add',
	templateUrl: '../views/albums-add.html',
	providers: [AlbumService]
})

export class AlbumsAddComponent implements OnInit {
	public titulo: string;
	public album: Album;
	public errorMessage: any;

	// formulario
	myForm: FormGroup;
	title: AbstractControl; 
	description: AbstractControl; 

	constructor(
			private _route: ActivatedRoute,
			private _router: Router,
			private _albumService: AlbumService,
			private fb: FormBuilder
		)
	{
		this.myForm = fb.group({
			'title': ['', Validators.required],
			'description': ['', Validators.required]
		});
		this.title = this.myForm.controls['title'];
		this.description = this.myForm.controls['description'];

		this.titulo = "Crear nuevo album";
	}

	ngOnInit(): void
	{
		console.log("Agregar album.");
		this.album = new Album(0, "", "");
	//	$('.ui.dropdown').dropdown('hide');
	}

	onSubmit(form: any): void
	{
		// console.log(" form value: ", form);
		this.album.title = form.title;
		this.album.description = form.description;

		this._albumService.addAlbum(this.album)
		.subscribe(
			response => {
				if(!response)
				{
					alert("Error en el servidor");
				}
				else
				{
					alert("Album creado correctamente");
					this._router.navigate(['/']);
				}
			},
			error => {
				this.errorMessage = <any>error;

				if(this.errorMessage != null)
				{
					console.log(this.errorMessage);
				}
			}
		);

		// this.myForm.reset();
	}

	public onDropdown(): void
	{
	}	
}