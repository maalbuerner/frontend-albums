import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AlbumsListComponent } from './components/albums-list.component';
import { AlbumsAddComponent } from './components/albums-add.component';
import { AlbumsDetailComponent } from './components/albums-detail.component';
import { AlbumsEditComponent } from './components/albums-edit.component';
import { ImagesAddComponent } from './components/images-add.component';
import { ImagesEditComponent } from './components/images-edit.component';
import { ImagesDetailComponent } from './components/images-detail.component';
import { routing, AppRoutingProviders } from './app.routing';

@NgModule({
  declarations: [
    AppComponent,
	  AlbumsListComponent,
    AlbumsAddComponent,
    AlbumsDetailComponent, 
    AlbumsEditComponent,
    ImagesAddComponent,
    ImagesEditComponent,
    ImagesDetailComponent    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    routing,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [AppRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
