import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsListComponent } from './components/albums-list.component';
import { AlbumsAddComponent } from './components/albums-add.component';
import { AlbumsDetailComponent } from './components/albums-detail.component';
import { AlbumsEditComponent } from './components/albums-edit.component';

import { ImagesAddComponent } from './components/images-add.component';
import { ImagesEditComponent } from './components/images-edit.component';
import { ImagesDetailComponent } from './components/images-detail.component';

const appRoutes: Routes = [
	{ path: '', component: AlbumsListComponent },
	{ path: 'add-album', component: AlbumsAddComponent },	
	{ path: 'album/:id', component: AlbumsDetailComponent },	
	{ path: 'edit-album/:id', component: AlbumsEditComponent },	

	{ path: 'add-image/:album_id', component: ImagesAddComponent },	
	{ path: 'edit-image/:id', component: ImagesEditComponent },	
	{ path: 'image/:id', component: ImagesDetailComponent },	
	{ path: '**', component: AlbumsListComponent }
];

export const AppRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);